// network ranges from https://uit.stanford.edu/guide/lna/network-numbers

variable "ipv4" {
  type    = "map"
  default = {
    livermore = [
      "204.63.224.0/21"
    ],
    stanford = [
      "171.64.0.0/14"
    ],
    residences = [
      "128.12.0.0/16"
    ],
    srwc = [
      "171.66.32.0/19"
    ],
    vpn = [
      "171.66.16.0/21",
      "171.66.24.0/21",
      "171.66.176.0/20"
    ],
    paw = [
      "171.67.52.0/23"
    ]
  }
}

variable "ipv6" {
  type    = "map"
  default = {
    stanford = [
      "2607:f6d0::/32"
    ]
  }
}


