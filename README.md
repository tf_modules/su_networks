Simple Terraform module to provide pre-defined Stanford University network ranges.

## Configuration

This module requires no variables / parameters to be defined:

    module "su_networks" {
      source = "git::https://code.stanford.edu/tf_modules/su_networks.git"
    }

## Usage

    // allow SSH access from main campus
    resource "aws_security_group_rule" "example_sg_rule" {
      description       = "Allow all inbound SSH from campus (IPv4+IPv6)"
      security_group_id = "${aws_security_group.example_sg.id}"
      type              = "ingress"
      from_port         = 2
      to_port           = 22
      protocol          = "tcp"
      cidr_blocks       = ["${module.su_networks.su_campus_ipv4}"]
      ipv6_cidr_blocks  = ["${module.su_networks.su_campus_ipv6}"]
   }


## Versioning

Normally you'll want the latest version, from the master branch. If
you want to use an older version, you can add the `ref` parameter to
the `git` URL:

    module "su_networks" {
      source = "git://code.stanford.edu/tf_modules/su_networks.git?ref=v1.0.0"
    }
