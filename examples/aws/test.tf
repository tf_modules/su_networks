provider "aws" {
  // use the default profile, or override it with the next line
  // profile = "..."
  region = "us-west-2"
}

module "su_networks" {
  source = "../../"
}

resource "aws_default_vpc" "default" {
}

resource "aws_security_group" "example_sg" {
  name        = "example_sg"
  description = "Example Security Group"
  vpc_id      = "${aws_default_vpc.default.id}"
}

// allow SSH access from main campus
resource "aws_security_group_rule" "example_sg_rule" {
  description       = "Allow all inbound SSH from campus (IPv4+IPv6)"
  security_group_id = "${aws_security_group.example_sg.id}"
  type              = "ingress"
  from_port         = 2
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${module.su_networks.su_campus_ipv4}"]
  ipv6_cidr_blocks  = ["${module.su_networks.su_campus_ipv6}"]
}

