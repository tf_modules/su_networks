# AWS Security Group Example

This example creates a new security group (_example\_sg_) in the
default VPC of your default AWS account... so you probably don't want
to run `terraform apply`!

## Initialize Terraform

    % terraform init
    Initializing modules...
    - module.su_networks
      Getting source "../../"
    
    Initializing provider plugins...
    - Checking for available provider plugins on https://releases.hashicorp.com...
    - Downloading plugin for provider "aws" (2.14.0)...
    ...

## Run `terraform plan` to see what would be created

    % terraform plan
    ...
    [ output about default vpc redacted for clarity ]
    [ output about example_sg redacted for clarity ]
    ...
      + aws_security_group_rule.example_sg_rule
      id:                               <computed>
      cidr_blocks.#:                    "1"
      cidr_blocks.0:                    "171.64.0.0/14"
      description:                      "Allow all inbound SSH from campus (IPv4+IPv6)"
      from_port:                        "2"
      ipv6_cidr_blocks.#:               "1"
      ipv6_cidr_blocks.0:               "2607:f6d0::/32"
      protocol:                         "tcp"
      security_group_id:                "${aws_security_group.example_sg.id}"
      self:                             "false"
      source_security_group_id:         <computed>
      to_port:                          "22"
      type:                             "ingress"
    ...

In the output from `terraform plan` you should see one `cidr_block`
definition (_171.64.0.0/14_)and one `ipv6_cidr_block`
(_2607:f6d0::/32_).

