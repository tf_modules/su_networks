module "su_networks" {
  source = "../../"
}

output "test_su_nets_all" {
  value = "${module.su_networks.su_all_nets}"
}

  
output "test_su_itlab_net" {
  value = "${cidrsubnet(module.su_networks.su_campus_ipv4[0], 13, 4402)}"
}
