# Simple Example

This example just defines some outputs to show the definitions from the module.

## Initialize Terraform

    % terraform init
    Initializing modules...
    - module.su_networks
      Getting source "../../"
    
    Terraform has been successfully initialized!
    ...

## Run `terraform apply` to see the outputs

Normally, you wouldn't want to run `terraform apply` in an example,
but in this case there are no resources to create, and it's the only
way to see the outputs.

    % terraform apply

    Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

    Outputs:

    test_su_itlab_net = 171.66.38.64/27
    test_su_nets_all = [
        171.64.0.0/14,
        2607:f6d0::/32,
        128.12.0.0/16,
        204.63.224.0/21
    ]

There should be two outputs: `test_su_nets_all` should be an array (list) of 3 IPv4 CIDR ranges (Campus, Residences, Livermore) and 1 IPv6 CIDR range (Stanford); `test_su_itlab_net` should show `171.66.38.64.27` - it's generated using the `campus_ipv4` list and the Terraform [cidrsubnet](https://www.terraform.io/docs/configuration/functions/cidrsubnet.html) function.

