// network ranges from https://uit.stanford.edu/guide/lna/network-numbers

output "su_all_nets" {
  value = [
    "${var.ipv4["stanford"]}",
    "${var.ipv6["stanford"]}",
    "${var.ipv4["residences"]}",
    "${var.ipv4["livermore"]}"
  ]
}
  
output "su_all_ipv4_nets" {
  value = [
    "${var.ipv4["stanford"]}",
    "${var.ipv4["residences"]}",
    "${var.ipv4["livermore"]}"
  ]
}

output "su_all_ipv6_nets" {
  value = [ "${var.ipv6["stanford"]}" ]
}

output "su_livermore" {
  value = [ "${var.ipv4["livermore"]}" ]
}

output "su_campus_all" {
  value = [ "${var.ipv4["stanford"]}", "${var.ipv6["stanford"]}" ]
}

output "su_campus_ipv4" {
  value = [ "${var.ipv4["stanford"]}" ]
}

output "su_campus_ipv6" {
  value = [ "${var.ipv6["stanford"]}" ]
}

output "su_residences" {
  value = [ "${var.ipv4["residences"]}" ]
}

output "su_srwc" {
  value = [ "${var.ipv4["srwc"]}" ]
}

output "su_vpn" {
  value = [ "${var.ipv4["vpn"]}" ]
}

output "su_paw" {
  value = [ "${var.ipv4["paw"]}" ]
}
